package com.example.anjali.javafire.mapkotlin

import com.google.android.gms.maps.model.LatLng

interface CurrentLocationCallbacks {
    fun onMoveMapView(boolean: Boolean)
    fun currentLocation(latLng: LatLng,result: String )
}