package com.example.anjali.javafire.androidfire;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.anjali.javafire.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;

public class MainActivity extends BaseActivity implements View.OnClickListener {
    private EditText displayName;
    private ImageView userPic;

    @Override
    protected void onLayoutCreated() {
        findViewById(R.id.btn_change_pic).setOnClickListener(this);
        findViewById(R.id.profile_done).setOnClickListener(this);
        displayName=findViewById(R.id.tv_displayname);
        userPic=findViewById(R.id.user_pic);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.profile_done:
                saveUserInformation();
                break;
            case R.id.btn_change_pic:
                pickGalleryImage(userPic);
                break;
        }
    }

    private void saveUserInformation() {
        String name= displayName.getText().toString();
        if(name.isEmpty()){
            displayName.setError("Name Required");
            displayName.requestFocus();
            return;
        }
        FirebaseUser firebaseUser=MyFireBaseAuth.getCurrentUser();
        if(firebaseUser!=null&& selectedImageUri !=null){
            UserProfileChangeRequest profileChangeRequest=
                    new UserProfileChangeRequest.Builder()
                            .setDisplayName(name)
                    .setPhotoUri(selectedImageUri).build();

            firebaseUser.updateProfile(profileChangeRequest)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                       if(task.isSuccessful()){
                           showToast("Profile Updated");
                       }
                        }
                    });
        }
    }


}
