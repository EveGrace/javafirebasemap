package com.example.anjali.javafire.maps;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by anjali on 14/3/18.
 */

public interface CurrentLocationCallbacks {
    void CurrentLocation(LatLng latLng,String result);
    void onMoveMapView(boolean isMove);
}
