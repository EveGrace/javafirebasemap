package com.example.anjali.javafire.kotlinfire

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Patterns
import android.view.View
import android.widget.EditText
import com.example.anjali.javafire.R
import com.google.firebase.auth.FirebaseAuthUserCollisionException
import kotlinx.android.synthetic.main.activity_kotlin_sign.*
import org.jetbrains.anko.design.longSnackbar
import org.jetbrains.anko.startActivity

class KotlinSignActivity : AppCompatActivity(), View.OnClickListener {
    lateinit var kt_UserEmail: EditText
    lateinit var kt_UserPassword: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_kotlin_sign)
        kt_btn_signup.setOnClickListener(this)
        kt_text_login.setOnClickListener(this)
        kt_UserEmail = findViewById(R.id.kt_signup_user_id)
        kt_UserPassword = findViewById(R.id.kt_signup_user_password)

    }

    override fun onClick(view: View?): Unit {
        when (view?.id) {
            R.id.kt_text_login -> {
                startActivity<KotlinLoginActivity>()
            }
            R.id.kt_btn_signup -> {
                createUser();
            }
        }
    }

    private fun createUser() {
        var email: String = kt_UserEmail.text.toString().trim()
        var password: String = kt_UserPassword.text.toString().trim()
        if (email.isEmpty()) {
            longSnackbar(Signup_parent, "Please provide email");
            kt_UserEmail.requestFocus()
            return
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            kt_UserEmail.setError("Please Enter valid email!");
            kt_UserEmail.requestFocus();
            return;
        }


        if (password.isEmpty()) {
            longSnackbar(Signup_parent, "Please provide password");
            kt_UserEmail.requestFocus()
            return
        }
        if (password.length<6) {
            longSnackbar(Signup_parent, "Please Enter valid password");
            kt_UserEmail.requestFocus()
            return
        }
        val fireBaseAuthInstance = FireBaseUtill.getFireBaseAuthInstance()
        fireBaseAuthInstance?.createUserWithEmailAndPassword(email,password)?.addOnCompleteListener{
            if (it.isSuccessful()) {
                startActivity<KotlinMainActivity>();

                longSnackbar(Signup_parent, "User Register Successfull");

            } else {
                if (it.exception is FirebaseAuthUserCollisionException) {
                    longSnackbar(Signup_parent, "Email is Already Register");
                }else{
                    longSnackbar(Signup_parent, it.exception?.message.toString());

                }

            }
        }
    }
}
