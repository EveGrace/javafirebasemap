package com.example.anjali.javafire.maps;

import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.anjali.javafire.R;
import com.example.anjali.javafire.androidfire.BaseActivity;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;

public class MapActivity extends BaseActivity implements MapManager.LocationCallbacksOnCameraMove {
    private SupportMapFragment mapFragment;
    private ImageView customMarker;
    private TextView tv_result;
    public static ProgressBar loadingView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        if (Build.VERSION.SDK_INT < 22)
//            setStatusBarTranslucent(false);
//        else
//            setStatusBarTranslucent(true);
        setContentView(R.layout.activity_map);
        if (!isGooglePlayServicesAvailable()) {
            showToast("No google Play services Available");
            finish();
        }
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        if (mapManager == null) {
            mapManager = MapManager.getMapInstance();
            mapManager.init(this);
            mapManager.setSupportMapFragment(mapFragment);
            mapManager.setLocationCallbacksOnCameraMove(this);
        } else {
            mapManager.setSupportMapFragment(mapFragment);
            mapManager.setLocationCallbacksOnCameraMove(this);

        }

    }
    @Override
    protected void onLayoutCreated() {

        customMarker=findViewById(R.id.custom_marker);
        tv_result=findViewById(R.id.tv_result);
        loadingView=findViewById(R.id.loading_bar);
    }


    private boolean isGooglePlayServicesAvailable() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, this, 0).show();
            return false;
        }
    }

    @Override
    public void locationOnMoveCamera(LatLng latLng,String result) {
        Log.d("Chawla", "CurrentLocation: "+latLng.latitude +"    "+latLng.longitude +"  "+result);
        tv_result.setText(result);
    }

    @Override
    public void mapViewMove(boolean isMove) {
        if(isMove){
            MapActivity.loadingView.setVisibility(View.VISIBLE);
        }else{
            MapActivity.loadingView.setVisibility(View.GONE);
        }
    }

    protected void setStatusBarTranslucent(boolean makeTranslucent) {
        if (makeTranslucent) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
    }
}
