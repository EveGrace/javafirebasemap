package com.example.anjali.javafire.contactfetchkotlin

import android.database.Cursor

object KotlinColumnMapper {

    fun mapDiplayName(cursor: Cursor, contact: KotlinContact, columnIndex: Int) {
        val displayName = cursor.getString(columnIndex)
        if (displayName != null && !displayName.isEmpty()) contact.mDisplayName = displayName
    }

    fun mapPhone(cursor: Cursor, contact: KotlinContact, columnIndex: Int){
       var phonenumber =cursor.getString(columnIndex)
        if (phonenumber != null && !phonenumber.isEmpty())
            phonenumber=phonenumber.replace("\\s","")
            contact.mPhoneNumbers.add(phonenumber)
    }

    fun mapEmail(cursor: Cursor?, contact: KotlinContact, columnIndex: Int) {
        val email = cursor?.getString(columnIndex)
        if (email != null && !email!!.trim({ it <= ' ' }).isEmpty()) {
            contact.mEmails.add(email)
        }
    }
}