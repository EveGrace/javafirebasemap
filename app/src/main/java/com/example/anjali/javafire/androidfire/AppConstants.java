package com.example.anjali.javafire.androidfire;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.text.TextUtils;
import java.util.Random;

/**
 * Created by Anjali Chawla on 26/10/17.
 */

public class AppConstants {
    private static String db="javafirebase";
    private static SharedPreferences pref;
    private static SharedPreferences.Editor editor;
    private static Context context;

    public static void init(Context context) {
        AppConstants.context = context;
    }

    public static boolean hasInternetConnection(Context baseActivity) {
        ConnectivityManager cm = (ConnectivityManager) baseActivity.getApplicationContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();
    }

    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        }
        return capitalize(model);
    }

    public static char getRandChar() {
        Random rnd = new Random();
        String randomLetters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        char c = randomLetters.charAt(rnd.nextInt(randomLetters.length()));
        return c;
    }

    private static final String ALLOWED_CHARACTERS = "0123456789qwertyuiopasdfghjklzxcvbnm";

    private static String getRandomStringForBarCode() {
        final Random random = new Random();
        final StringBuilder sb = new StringBuilder(4);
        for (int i = 0; i < 4; ++i)
            sb.append(ALLOWED_CHARACTERS.charAt(random.nextInt(ALLOWED_CHARACTERS.length())));
        return sb.toString();
    }

    /* no-op */
    private static String capitalize(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        char[] arr = str.toCharArray();
        boolean capitalizeNext = true;
        String phrase = "";
        for (char c : arr) {
            if (capitalizeNext && Character.isLetter(c)) {
                phrase += Character.toUpperCase(c);
                capitalizeNext = false;
                continue;
            } else if (Character.isWhitespace(c)) {
                capitalizeNext = true;
            }
            phrase += c;
        }
        return phrase;
    }


    public static void insertIntoSharedPref(String key, String value) {
        pref = context.getSharedPreferences(db, context.MODE_PRIVATE);
        editor = pref.edit();
        editor.putString(key, value);
        editor.commit();
    }


    public static String getSharedPrefData(Context mContext, String key) {
        try {
            pref = mContext.getSharedPreferences(db, mContext.MODE_PRIVATE);
            return pref.getString(key, "{}");
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }





}
