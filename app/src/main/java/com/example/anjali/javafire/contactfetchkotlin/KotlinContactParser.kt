package com.example.anjali.javafire.contactfetchkotlin

import android.content.ContentResolver
import android.content.Context
import android.database.Cursor
import android.net.Uri
import android.provider.ContactsContract
import android.support.v4.util.LongSparseArray
import io.reactivex.Observable
import io.reactivex.ObservableEmitter
import io.reactivex.ObservableOnSubscribe

class KotlinContactParser(context: Context) {

    val mContentResolver: ContentResolver

    init {
        mContentResolver = context.contentResolver
    }

    companion object {

        private val EMAIL_PROJECTION = arrayOf(ContactsContract.CommonDataKinds.Email.CONTACT_ID, ContactsContract.CommonDataKinds.Email.DATA)
        private val NUMBER_PROJECTION = arrayOf(ContactsContract.CommonDataKinds.Phone.CONTACT_ID, ContactsContract.CommonDataKinds.Phone.NUMBER)

        val CONTACT_CONTENT_URI: Uri? = ContactsContract.Contacts.CONTENT_URI
         val EMAIL_CONTENT_URI = ContactsContract.CommonDataKinds.Email.CONTENT_URI

        val PHONE_CONTENT_URI: Uri? = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        val DISPLAY_NAME: String = ContactsContract.Contacts.DISPLAY_NAME_PRIMARY
        val HAS_PHONE_NUMBER: String = ContactsContract.Contacts.HAS_PHONE_NUMBER

        private val PROJECTION = arrayOf<String>(ContactsContract.Contacts._ID,
        ContactsContract.Contacts.IN_VISIBLE_GROUP,
                DISPLAY_NAME,
        ContactsContract.Contacts.STARRED,
        ContactsContract.Contacts.PHOTO_URI,
        ContactsContract.Contacts.PHOTO_THUMBNAIL_URI,
        HAS_PHONE_NUMBER)

        fun fetch(mContext: Context): Observable<KotlinContact> {
            return Observable.create(ObservableOnSubscribe {
                KotlinContactParser(mContext).fetch(it)
            })
        }
    }


    private fun fetch(emitter: ObservableEmitter<KotlinContact>) {
        val contactArray = LongSparseArray<KotlinContact>()
        val cursor = createCursor()
        cursor?.moveToFirst()
        val idColumnIndex = cursor!!.getColumnIndex(ContactsContract.Contacts._ID)
        val displayNamePrimaryColumnIndex = cursor.getColumnIndex(DISPLAY_NAME)
        while (!cursor.isAfterLast) {
            val id = cursor.getLong(idColumnIndex)
            var kotlinContact = contactArray.get(id)
            if (kotlinContact == null) {
                kotlinContact = KotlinContact(id)
                KotlinColumnMapper.mapDiplayName(cursor, kotlinContact, displayNamePrimaryColumnIndex)

                val hasPhoneNumber = cursor.getInt(cursor.getColumnIndex(HAS_PHONE_NUMBER))

                val emailCursor = mContentResolver.query(EMAIL_CONTENT_URI, EMAIL_PROJECTION,
                        ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?", arrayOf(id.toString()), null)

                if (emailCursor != null) {
                    emailCursor.moveToFirst()
                    val emailDataColumnIndex = emailCursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA)
                    while (!emailCursor.isAfterLast()) {
                        KotlinColumnMapper.mapEmail(emailCursor, kotlinContact, emailDataColumnIndex)
                        emailCursor.moveToNext()
                    }
                    emailCursor.close()
                }

                if (hasPhoneNumber > 0) {
                    val phoneCursor = mContentResolver.query(PHONE_CONTENT_URI, NUMBER_PROJECTION,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            arrayOf(id.toString()), null)
                    if (phoneCursor != null) {
                        phoneCursor.moveToFirst()
                        val phoneNumberColumnIndex = phoneCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)
                        while (!phoneCursor.isAfterLast) {
                            KotlinColumnMapper.mapPhone(phoneCursor, kotlinContact, phoneNumberColumnIndex)
                            phoneCursor.moveToNext()
                        }
                        phoneCursor.close()
                    }
                }

                contactArray.put(id, kotlinContact)
            }
            cursor.moveToNext()
            emitter.onNext(kotlinContact)
        }
        cursor.close()
        emitter.onComplete()
    }

    private fun createCursor(): Cursor? {
        return mContentResolver.query(CONTACT_CONTENT_URI, PROJECTION, null, null, ContactsContract.Contacts._ID);
    }


}