package com.example.anjali.javafire.androidfire;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;

import com.example.anjali.javafire.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class LoginActivity extends BaseActivity implements View.OnClickListener {

    EditText userEmail;
    EditText userPassword;
    FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mAuth = MyFireBaseAuth.getInstance();
    }

    @Override
    protected void onLayoutCreated() {
        findViewById(R.id.text_signup).setOnClickListener(this);
        findViewById(R.id.btn_login).setOnClickListener(this);
        userEmail = findViewById(R.id.user_id);
        userEmail.setText("chawla.anjali5@gmail.com");
        userPassword = findViewById(R.id.user_password);
        userPassword.setText("123456");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.text_signup:
                startActivity(new Intent(this, SignupActivity.class));
                finish();
                break;
            case R.id.btn_login:
                userLogin();
                break;
        }
    }

    private void userLogin() {
        String email = userEmail.getText().toString().trim();
        String password = userPassword.getText().toString().trim();
        if (email.isEmpty()) {
            userPassword.setError("Email is required!");
            userEmail.requestFocus();
            return;
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            userPassword.setError("Please Enter valid email!");
            userEmail.requestFocus();
            return;
        }
        if (password.isEmpty()) {
            userPassword.setError("Password is required!");
            userEmail.requestFocus();
            return;
        }

        if (password.length() < 6) {
            userPassword.setError("Password length should be more than 6!");
            userEmail.requestFocus();
            return;
        }
        showProgress();
        mAuth.signInWithEmailAndPassword(email, password).
                addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        hideProgress();
           if(task.isSuccessful()){
               startActivity(new Intent(LoginActivity.this,MainActivity.class));
               showToast("Success Login");
           }else{
               showToast(task.getException().getMessage());
           }
                    }
                });
    }
}
