package com.example.anjali.javafire;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import java.util.ArrayList;


@SuppressLint("Registered")
public class AppPermissionChecker extends Activity {
    class AppPermission {
        String permission;
        int requestCode;
        boolean isGranted;
    }

    interface PermissionCheckerCallback {
        void permissionListener(String name, boolean isGranted);
    }

    int rc = 0;

    private Context context;
    private PermissionCheckerCallback permissionCheckerCallback;
    //    private ArrayList<AppPermission> appPermissions = new ArrayList<>();
    private ArrayList<AppPermission> appPermissionsM = new ArrayList<>();

    public AppPermissionChecker(Context context) {
        this.context = context;
    }

    public AppPermissionChecker(Context context, PermissionCheckerCallback permissionCheckerCallback) {
        this.context = context;
        this.permissionCheckerCallback = permissionCheckerCallback;
    }

//    public void setAppPermissionsList(ArrayList<String> list) {
//        for (String perms : list) {
//            AppPermission appPermission = new AppPermission();
//            appPermission.permission = perms;
//            appPermission.requestCode = rc;
//            appPermissions.add(appPermission);
//            rc++;
//        }
//    }

    public void setAppPermissionsList(String... list) {
        for (int i = 0; i < list.length; i++) {
            AppPermission appPermission = new AppPermission();
            appPermission.permission = list[i];
            appPermission.requestCode = i;
            int isGrantedInt = ContextCompat.checkSelfPermission(context, list[i]);
            if (isGrantedInt == PackageManager.PERMISSION_GRANTED)
                appPermission.isGranted = true;

            appPermissionsM.add(appPermission);
        }


//        for (String perms : list) {
//            AppPermission appPermission = new AppPermission();
//            appPermission.permission = perms;
//            appPermission.requestCode = list;
//            appPermissions.add(appPermission);
//            rc++;
//        }
    }

    /**
     * new request for permission check
     *
     * @return is All permission granted!
     */
    public boolean isAllPermissionGranted() {
        for (AppPermission appPermission : appPermissionsM) {
            int i = ContextCompat.checkSelfPermission(context, appPermission.permission);
            if (i != PackageManager.PERMISSION_GRANTED) {
                return false;
            }

        }

        return true;
    }

    /**
     * request to activity context for remening ungranted permissions
     */
    public void requestForAllPermission() {
        for (AppPermission appPermission : appPermissionsM) {
            String[] myarray = new String[appPermissionsM.size()];
            for (int i = 0; i <= appPermissionsM.size() - 1; i++) {
                myarray[i] = appPermissionsM.get(i).permission;
            }

            ActivityCompat.requestPermissions((Activity) context, myarray, 101);

        }
    }


    public static boolean checkForWritePermission(Context baseActivity) {
        int permissionCheck = ContextCompat.checkSelfPermission(baseActivity, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return permissionCheck == PackageManager.PERMISSION_GRANTED;
    }



}
