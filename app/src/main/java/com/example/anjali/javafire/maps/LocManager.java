package com.example.anjali.javafire.maps;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.example.anjali.javafire.androidfire.BaseActivity;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import static android.content.Context.LOCATION_SERVICE;

/**
 * Created by anjali on 14/3/18.
 */

public class LocManager implements LocationListener, GoogleClientManager.ConnectionListener {
    public static final String TAG = "LocationManager";
    private static LocManager locManager;
    private LocationManager locationManager;
    private BaseActivity baseActivity;
    private LocationRequest mLocationRequest;
    private static final long INTERVAL = 1000 * 15;
    private static final long FASTEST_INTERVAL = 1000 * 10;
    private GoogleClientManager googleClientManager;


    private LocManager() {
    }

    public void build(BaseActivity baseActivity) {
        this.baseActivity = baseActivity;
        googleClientManager = new GoogleClientManager(baseActivity);
        googleClientManager.setConnectionListener(this);
        locationManager = (LocationManager) baseActivity.getSystemService(LOCATION_SERVICE);
        createLocationRequest();
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, "onLocationChanged");
        Log.d(TAG, getLocationProvider());
        if (MapManager.isMapReady) {
            // update the activity where location is needed
        }
    }


    public static LocManager getLocManagerInstance() {
        if (locManager == null) {
            return new LocManager();
        }
        return locManager;
    }

    private String getLocationProvider() {
        if (locationManager != null) {
            Criteria criteria = new Criteria();
            criteria.setAccuracy(Criteria.ACCURACY_FINE);
            return locationManager.getBestProvider(criteria, true);
        }
        return "no Provider";
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        baseActivity.showToast("Error conn. to GoogleClient" + connectionResult.getErrorMessage());
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG, "onConnectionSuspended: GoogleClient");
    }

    @Override
    public void onConnected(Bundle bundle) {
        startLocationUpdates();
        baseActivity.showToast("Connecting to Google client");
    }

    private void startLocationUpdates() {
        new Thread() {
            public void run() {
                if (ActivityCompat.checkSelfPermission(baseActivity,
                        Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                        ActivityCompat.checkSelfPermission(baseActivity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                PendingResult<Status> pendingResult = LocationServices.FusedLocationApi.requestLocationUpdates(
                        googleClientManager.getGoogleApiClient(), mLocationRequest, LocManager.this);
                Log.d(TAG, "Location update started ..............: ");
            }
        }.start();

    }

    @SuppressLint("RestrictedApi")
    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }


    /* private void requestForLocation() {
        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_COARSE);

        String provider = locationManager.getBestProvider(criteria, true);
        if (provider == null) {
            provider = LocationManager.NETWORK_PROVIDER;
        } else {
            provider = LocationManager.GPS_PROVIDER;
        }
        if (ActivityCompat.checkSelfPermission(baseActivity, Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(baseActivity,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            baseActivity.getLocationCOARSE_LOCATIONPermission();
            return;
        }
        locationManager.requestLocationUpdates
                (provider, 1000, 2, this);
    }*/

    public interface CurrentLocationCallbacks {
        void currentLocation(double latitude, double longitude);
    }

}
