package com.example.anjali.javafire.kotlinfire

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.anjali.javafire.R

class KotlinMainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_kotlin_main)
    }
}
