package com.example.anjali.javafire.contactfetchkotlin

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.anjali.javafire.R
import kotlinx.android.synthetic.main.list_row_contact_pick_item.view.*

class KotlinContactAdapter(val contactList: List<KotlinContact>) : RecyclerView.Adapter<KotlinContactAdapter.KotlinContactViewHolder>() {
//    var mContactList: List<KotlinContact>
    var mCloneList: List<KotlinContact> =contactList

//    init {
//        this.mContactList = contactList
//        this.mCloneList = contactList
//    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): KotlinContactViewHolder {
        val itemView: View = LayoutInflater.from(parent.context).inflate(R.layout.list_row_contact_pick_item, parent, false)
        return KotlinContactViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return contactList.size
    }

    override fun onBindViewHolder(holder: KotlinContactViewHolder, position: Int) {
        val contact=contactList.get(position)
        holder.bindItems(contact)
    }


    class KotlinContactViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(contact: KotlinContact) {

            val name=contact.mDisplayName
            itemView.tvContactName.text=name
            itemView.vRoundLetterView.titleText=name?.substring(0,1)?.toUpperCase()

            if(contact.mPhoneNumbers.size>0){
                val phoneNumber =contact.mPhoneNumbers.get(0).replace("\\s","")
                val displayName=contact.mDisplayName?.replace("\\s","")
                if(!phoneNumber.equals(displayName)){
                    itemView.tvNumber?.visibility=View.VISIBLE
                    itemView.tvNumber?.text=phoneNumber
                    System.out.println("phoneNumber "+phoneNumber)
                }else{
                    itemView.tvNumber?.visibility=View.GONE
                }
            } else run {
                if (contact.mEmails.size > 0) {
                    val email = contact.mEmails.get(0).replace("\\s+".toRegex(), "")
                    val displayName = contact.mDisplayName?.replace("\\s+".toRegex(), "")
                    if (email != displayName) {
                        itemView.tvNumber.setVisibility(View.VISIBLE)
                        itemView.tvNumber.setText(email)
                        System.out.println("phoneNumberemail "+email)

                    } else {
                        itemView.tvNumber.setVisibility(View.GONE)
                    }
                } else {
                    itemView.tvNumber.setVisibility(View.GONE)
                }
            }
        }

    }
}