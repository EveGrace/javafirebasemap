package com.example.anjali.javafire.kotlinfire

import com.google.firebase.auth.FirebaseAuth

object FireBaseUtill {
    private var mAuth: FirebaseAuth?=null

    fun getFireBaseAuthInstance(): FirebaseAuth? {
        if (mAuth == null) {
            mAuth = FirebaseAuth.getInstance()
            println("NewAuthInstance")
            return mAuth
        }
        println("OldAuthInstance")
        return mAuth;
    }
}