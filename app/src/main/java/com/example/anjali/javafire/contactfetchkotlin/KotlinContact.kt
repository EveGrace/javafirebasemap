package com.example.anjali.javafire.contactfetchkotlin

import android.graphics.Color

data class KotlinContact(var mId: Long) {
    var mDisplayName: String? = "";
    var mEmails: MutableList<String> = mutableListOf(String())
    var mPhoneNumbers: MutableList<String> = mutableListOf(String())
    var backgroundColor: Int = Color.BLUE
}

