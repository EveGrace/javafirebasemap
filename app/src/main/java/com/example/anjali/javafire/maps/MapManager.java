package com.example.anjali.javafire.maps;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;

import com.example.anjali.javafire.androidfire.BaseActivity;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.List;

public class MapManager implements OnMapReadyCallback, GoogleMap.OnCameraIdleListener, GoogleMap.OnCameraMoveStartedListener {
    private BaseActivity baseActivity;
    private SupportMapFragment supportMapFragment;
    private LocationCallbacksOnCameraMove currentLocationCallbacks;
    private GoogleMap mGoogleMap;
    private static MapManager mapManager;
public static boolean isMapReady= false;
    private MapManager() {

    }

    public void init(BaseActivity baseActivity) {
        this.baseActivity = baseActivity;
    }

    public void isLocationUpdationNeeded(boolean isNeeded){
        if(isNeeded){
            LocManager.getLocManagerInstance().build(baseActivity);
        }
    }

    public static MapManager getMapInstance() {
        if (mapManager == null) {
            return new MapManager();
        }
        return mapManager;
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        this.mGoogleMap = googleMap;
        isMapReady=true;
        baseActivity.showToast("Map ready");
        enableMyLocation();
    }



    public void enableMyLocation() {
        if (ActivityCompat.checkSelfPermission(baseActivity,
                Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(baseActivity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        baseActivity.mGoogleMap.setMyLocationEnabled(true);
    }


    public void setSupportMapFragment(SupportMapFragment supportMapFragment) {
        this.supportMapFragment = supportMapFragment;
        supportMapFragment.getMapAsync(this);
    }

    public void initOnCameraMoveStartedListener() {
        if (mGoogleMap != null) mGoogleMap.setOnCameraMoveStartedListener(this);

    }

    public void initOnCameraIdleListener() {
        if (mGoogleMap != null) mGoogleMap.setOnCameraIdleListener(this);

    }

    @Override
    public void onCameraIdle() {
        if (mGoogleMap != null) {
            mGoogleMap.clear();
        }
        getLocationOnCameraIdle();
    }

    private void getLocationOnCameraIdle() {
        new Thread() {
            @Override
            public void run() {
                LatLng latLng = mGoogleMap.getCameraPosition().target;

                Geocoder geocoder = new Geocoder(baseActivity);
                try {
                    List<Address> addressList = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
                    if (addressList != null && addressList.size() > 0) {
                        String locality = addressList.get(0).getAddressLine(0);
                        String country = addressList.get(0).getCountryName();
                        if (!locality.isEmpty() && !country.isEmpty())
                            currentLocationCallbacks.mapViewMove(false);
//                            currentLocationCallbacks.CurrentLocation(latLng,locality + "  " + country);
                        currentLocationCallbacks.locationOnMoveCamera(latLng, locality);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    @Override
    public void onCameraMoveStarted(int i) {
        currentLocationCallbacks.mapViewMove(true);
    }

    public void setLocationCallbacksOnCameraMove(LocationCallbacksOnCameraMove locationListener) {
        this.currentLocationCallbacks = locationListener;
    }
    public interface LocationCallbacksOnCameraMove {
        void locationOnMoveCamera(LatLng latLng, String result);

        void mapViewMove(boolean isMove);
    }

}
