package com.example.anjali.javafire.contactfetchkotlin

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import com.example.anjali.javafire.R
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Predicate
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_kotlin_contact.*
import java.util.*
import kotlin.Comparator

class KotlinContactActivity : AppCompatActivity(), SearchView.OnQueryTextListener {

    var contactList: MutableList<KotlinContact> = mutableListOf<KotlinContact>()
    lateinit var kotlinContactAdapter :KotlinContactAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_kotlin_contact)
        kt_search_view.setOnQueryTextListener(this)
        loadContact()
        kotlinContactAdapter= KotlinContactAdapter(contactList)
        kt_rv_contacts.layoutManager= LinearLayoutManager(this)
        kt_rv_contacts.adapter=kotlinContactAdapter
    }

    private fun loadContact() =
            KotlinContactParser.fetch(this)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .filter(object : Predicate<KotlinContact> {
                        override fun test(t: KotlinContact): Boolean {
                            return true
                        }
                    })
                    .subscribe(object : Observer<KotlinContact> {
                        override fun onSubscribe(d: Disposable) {
                        }

                        override fun onComplete() {
                        }

                        override fun onNext(t: KotlinContact) {
                            contactList.add(t)
                            Collections.sort(contactList,object :Comparator<KotlinContact>{
                                override fun compare(o1: KotlinContact?, o2: KotlinContact?): Int {
                                    return o1?.mDisplayName!!.compareTo(o2?.mDisplayName.toString(),true)
                                }
                            })
                            if(true){
                                kotlinContactAdapter.notifyDataSetChanged()
                            }
                        }

                        override fun onError(e: Throwable) {
                        }
                    })


    override fun onQueryTextSubmit(query: String?): Boolean {
        return false
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        return false
    }
}

