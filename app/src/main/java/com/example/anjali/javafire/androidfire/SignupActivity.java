package com.example.anjali.javafire.androidfire;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;

import com.example.anjali.javafire.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;

public class SignupActivity extends BaseActivity implements View.OnClickListener {
    private EditText userEmail;
    private EditText userPassword;
    private FirebaseAuth mAuth;

    @Override
    protected void onLayoutCreated() {
        userEmail = findViewById(R.id.signup_user_id);
        userPassword = findViewById(R.id.signup_user_password);
        findViewById(R.id.btn_signup).setOnClickListener(this);
        findViewById(R.id.text_login).setOnClickListener(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        mAuth = MyFireBaseAuth.getInstance();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_signup:
                registerUser();
                break;
            case R.id.text_login:
                startActivity(new Intent(this, LoginActivity.class));
                finish();
                break;

        }
    }

    private void registerUser() {
        String email = userEmail.getText().toString().trim();
        String password = userPassword.getText().toString().trim();
        if (email.isEmpty()) {
            userEmail.setError("Email is required!");
            userEmail.requestFocus();
            return;
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            userEmail.setError("Please Enter valid email!");
            userEmail.requestFocus();
            return;
        }
        if (password.isEmpty()) {
            userPassword.setError("Password is required!");
            userPassword.requestFocus();
            return;
        }

        if (password.length() < 6) {
            userPassword.setError("Password length should be more than 6!");
            userPassword.requestFocus();
            return;
        }
        showProgress();
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        hideProgress();
                        if (task.isSuccessful()) {
                            startActivity(new Intent(SignupActivity.this,MainActivity.class));

                            showToast("User Register Successfull");

                        } else {
                            if (task.getException() instanceof
                                    FirebaseAuthUserCollisionException) {
                                showToast("Email is Already Register");
                            }else{showToast(task.getException().getMessage());

                            }

                        }
                    }
                });
    }
}
