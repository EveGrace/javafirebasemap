package com.example.anjali.javafire.contactfetch;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.anjali.javafire.R;
import com.example.anjali.javafire.views.RoundLetterView;

class ContactViewHolder extends RecyclerView.ViewHolder {
    protected final View mView;
    protected TextView tvContactName;
    protected TextView tvNumber;
    protected RoundLetterView vRoundLetterView;
    protected ImageView ivSelectedState;
    ContactViewHolder(View view) {
        super(view);
        this.mView = view;
        this.vRoundLetterView = (RoundLetterView) view.findViewById(R.id.vRoundLetterView);
        this.tvContactName = (TextView) view.findViewById(R.id.tvContactName);
        this.tvNumber = (TextView) view.findViewById(R.id.tvNumber);
        this.ivSelectedState = (ImageView) view.findViewById(R.id.ivSelectedState);
    }
}
