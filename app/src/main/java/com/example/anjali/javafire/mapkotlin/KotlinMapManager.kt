package com.example.anjali.javafire.mapkotlin

import android.location.Address
import android.location.Geocoder
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import java.io.IOException


// class with secondary constructor
class KotlinMapManager(activity: KotlinMapActivity) : OnMapReadyCallback {

    // primary constructor
/* constructor(activity: KotlinMapActivity){
 }*/
    val activityMap = activity;
    lateinit var mapManagerFragment: SupportMapFragment
    private lateinit var mCallback: CurrentLocationCallbacks


    private var mGoogleMap: GoogleMap? = null

    override fun onMapReady(mGoogle: GoogleMap?) {
        print("Map ready ")
        this.mGoogleMap = mGoogle;
        mGoogleMap?.setOnCameraMoveListener {
            mCallback.onMoveMapView(true)
        }
        mGoogleMap?.setOnCameraIdleListener {
            if (mGoogleMap != null) {
                mGoogleMap!!.clear()
            }
            findLocation()
        }
    }

    private fun findLocation() {
        val latLng = mGoogleMap?.cameraPosition?.target
        val geocoder = Geocoder(activityMap)
        try {
            val addressList = geocoder.getFromLocation(latLng!!.latitude, latLng.longitude, 1)
            if(addressList!=null && addressList.size>0){
                val locality=addressList.get(0).getAddressLine(0);
                val country=addressList.get(0).countryName;
                if(!locality.isEmpty() && !country.isEmpty()){
                    mCallback.onMoveMapView(false)
                    mCallback.currentLocation(latLng,locality + "  " + country)
                }

            }
        } catch (exception: IOException) {
            exception.printStackTrace()
        }
    }

    fun setOnMovedLocationListener(callbacks: CurrentLocationCallbacks) {
        this.mCallback = callbacks
    }


    // normal member function
    /*fun setSupportMapFragment(mapFragment: SupportMapFragment) {
    }*/
}