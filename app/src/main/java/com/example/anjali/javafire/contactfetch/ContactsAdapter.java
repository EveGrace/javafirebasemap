package com.example.anjali.javafire.contactfetch;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.example.anjali.javafire.R;
import com.example.anjali.javafire.androidfire.BaseActivity;

import java.util.ArrayList;
import java.util.List;

public class ContactsAdapter extends RecyclerView.Adapter<ContactViewHolder> implements Filterable {
    private List<Contact> contactList;
    private List<Contact> cloneList;
    private ValueFilter valueFilter;

    ContactsAdapter(List<Contact> contactList) {
        this.contactList = contactList;
        this.cloneList = contactList;
    }


    @NonNull
    @Override
    public ContactViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_row_contact_pick_item, parent, false);

        return new ContactViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ContactViewHolder holder, int position) {
        Contact contact = contactList.get(position);
        String name = contact.getDisplayName();

        holder.tvContactName.setText(name);
        holder.vRoundLetterView.setTitleText(String.valueOf(name.charAt(0)));
        holder.vRoundLetterView.setBackgroundColor(contact.getBackgroundColor());
        if (contact.getPhoneNumbers().size() > 0) {
            String phoneNumber = contact.getPhoneNumbers().get(0).replaceAll("\\s+", "");
            String displayName = contact.getDisplayName().replaceAll("\\s+", "");
            if (!phoneNumber.equals(displayName)) {
                holder.tvNumber.setVisibility(View.VISIBLE);
                holder.tvNumber.setText(phoneNumber);
            } else {
                holder.tvNumber.setVisibility(View.GONE);
            }
        } else {
            if (contact.getEmails().size() > 0) {
                String email = contact.getEmails().get(0).replaceAll("\\s+", "");
                String displayName = contact.getDisplayName().replaceAll("\\s+", "");
                if (!email.equals(displayName)) {
                    holder.tvNumber.setVisibility(View.VISIBLE);
                    holder.tvNumber.setText(email);
                } else {
                    holder.tvNumber.setVisibility(View.GONE);
                }
            } else {
                holder.tvNumber.setVisibility(View.GONE);
            }
        }


    }

    @Override
    public int getItemCount() {
        return contactList.size();
    }

    @Override
    public Filter getFilter() {
        if (valueFilter == null) {
            valueFilter = new ValueFilter();
        }
        return valueFilter;
    }


    private class ValueFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            if (constraint != null && constraint.length() > 0) {
                List<Contact> filterList = new ArrayList<>();
                for (int i = 0; i < cloneList.size(); i++) {
                    Contact contact = cloneList.get(i);
                    if ((contact.getDisplayName().toUpperCase()).contains(constraint.toString().toUpperCase())) {
                        filterList.add(contact);
                    }
                    for (String s : contact.getPhoneNumbers()) {
                        if (s.contains(constraint.toString())) {
                            filterList.add(contact);
                        }
                    }
                    for (String s : contact.getEmails()) {
                        if (s.contains(constraint.toString())) {
                            filterList.add(contact);
                        }
                    }
                }
                results.count = filterList.size();
                results.values = filterList;
            } else {
                results.count = cloneList.size();
                results.values = cloneList;
            }
            return results;

        }

        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {
            contactList = (List<Contact>) results.values;
            notifyDataSetChanged();
        }

    }


}


