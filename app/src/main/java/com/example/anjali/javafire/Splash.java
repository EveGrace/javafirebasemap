package com.example.anjali.javafire;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.anjali.javafire.androidfire.BaseActivity;
import com.example.anjali.javafire.androidfire.IDIalogClick;
import com.example.anjali.javafire.androidfire.LoginActivity;
import com.example.anjali.javafire.contactfetch.ContactActivity;
import com.example.anjali.javafire.contactfetchkotlin.KotlinContactActivity;
import com.example.anjali.javafire.kotlinfire.KotlinLoginActivity;
import com.example.anjali.javafire.maps.MapActivity;

public class Splash extends BaseActivity implements View.OnClickListener {
    private AppPermissionChecker permissionChecker;

    @Override
    protected void onLayoutCreated() {
        permissionChecker = new AppPermissionChecker(this);
        permissionChecker.setAppPermissionsList(
                android.Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION);
        if (!permissionChecker.isAllPermissionGranted()) {
            showAlertDialog("", getString(R.string.permission_text), false, new IDIalogClick() {
                @Override
                public void okClick() {
                    permissionChecker.requestForAllPermission();
                }
            });
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        findViewById(R.id.btn_java).setOnClickListener(this);
        findViewById(R.id.btn_kotlin).setOnClickListener(this);
        findViewById(R.id.btn_map).setOnClickListener(this);
        findViewById(R.id.kt_btn_map).setOnClickListener(this);
        findViewById(R.id.btn_contact).setOnClickListener(this);
        findViewById(R.id.btn_kt_contact).setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_java:
                startActivity(new Intent(this, LoginActivity.class));
                break;
            case R.id.btn_contact:
                if (checkForReadContactPermissionTF())
                    startActivity(new Intent(this, ContactActivity.class));
                break;
            case R.id.btn_kotlin:
                startActivity(new Intent(this, KotlinLoginActivity.class));
                break;
            case R.id.btn_map:
                if (getLocationPermission()) {
                    startActivity(new Intent(this, MapActivity.class));
                }
                break;
            case R.id.kt_btn_map:
                if (getLocationPermission()) {
                    startActivity(new Intent(this, MapActivity.class));
                }
                break;
            case R.id.btn_kt_contact:
                if (getLocationPermission()) {
                    startActivity(new Intent(this, KotlinContactActivity.class));
                }
                break;
        }
    }
}
