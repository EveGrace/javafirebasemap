package com.example.anjali.javafire.mapkotlin

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import com.example.anjali.javafire.R
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GooglePlayServicesUtil
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import kotlinx.android.synthetic.main.activity_kotlin_map.*
import org.jetbrains.anko.design.longSnackbar
import org.jetbrains.anko.find

class KotlinMapActivity : AppCompatActivity(), CurrentLocationCallbacks {


    private lateinit var mapFragment: SupportMapFragment;
    private lateinit var customMarker: ImageView
    private lateinit var tv_result: TextView
    private lateinit var loadingView: ProgressBar
    private lateinit var mapManager: KotlinMapManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_kotlin_map)
        if (!isGooglePlayServicesAvailable()) {
            longSnackbar(kt_map_parent, "No google Play services Available")
            finish()
        }
        mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment

        mapManager = KotlinMapManager(this)
        mapManager.setSupportMapFragment(mapFragment)
        mapManager.setOnMovedLocationListener(this)
        initView()

    }

    private fun initView() {
        customMarker = find(R.id.kt_custom_marker)
        tv_result = find(R.id.kt_tv_result)
        loadingView = find(R.id.kt_loading_bar)
        loadingView.visibility = View.GONE
    }

    private fun isGooglePlayServicesAvailable(): Boolean {
        val status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, this, 0).show();
            return false;
        }
    }

    override fun currentLocation(latLng: LatLng, result: String) {
        tv_result.text = result;
    }

    override fun onMoveMapView(boolean: Boolean) {
        if (boolean) {
            loadingView.visibility = View.VISIBLE
        } else {
            loadingView.visibility = View.GONE
        }
    }

}

// extension function of kotlinMapmanager in This activity made outside of classs
private fun KotlinMapManager.setSupportMapFragment(mapFragment: SupportMapFragment) {
    mapManagerFragment = mapFragment;
    mapManagerFragment.getMapAsync(this)
}
